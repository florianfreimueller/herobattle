package net.sleiferhd.herobattle.countdown;

public abstract class Countdown {
	int seconds;
	
	public abstract void start();
	public abstract void stop();
}
