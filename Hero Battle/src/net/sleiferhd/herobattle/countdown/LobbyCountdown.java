package net.sleiferhd.herobattle.countdown;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

import net.sleiferhd.herobattle.HeroBattle;
import net.sleiferhd.herobattle.gamestates.GameState;

public class LobbyCountdown extends Countdown{
	
	private int taskID;
	private boolean repeat;
	
	@Override
	public void start(){
		seconds = 60*5;
		float expLeiste = seconds;
		repeat = false;
		taskID = Bukkit.getScheduler().scheduleSyncRepeatingTask(HeroBattle.getBattle(), new Runnable() {
			
			@SuppressWarnings("deprecation")
			@Override
			public void run() {
				for(Player p : HeroBattle.getBattle().getPlayersIngame()){
					p.setLevel(seconds);
					p.setExp((float)(60*5)/expLeiste);
				}
					switch(seconds){
					case 5*60:
						Bukkit.broadcastMessage("�7[�5Hero�fBattle�7] �6Das Spiel startet in 5 Minuten.");
						for(Player p : HeroBattle.getBattle().getPlayersIngame()){
							p.sendTitle("�5Hero �fGames", "�6startet in 5 Minuten");
						}
						break;
					case 60:
						Bukkit.broadcastMessage("�7[�5Hero�fBattle�7] �6Das Spiel startet in einer Minute.");
						for(Player p : HeroBattle.getBattle().getPlayersIngame()){
							p.sendTitle("�5Hero �fGames", "�6startet in einer Minute");
						}
						break;
					case 30:
					case 10:
					case 5:
					case 4:
					case 3:
					case 2:
						Bukkit.broadcastMessage("�7[�5Hero�fBattle�7] �6Das Spiel startet in " +seconds+ " Sekunden.");
						for(Player p : HeroBattle.getBattle().getPlayersIngame()){
							p.sendTitle("�5Hero �fGames", "�6startet in " + seconds + " Sekunden");
						}
						break;
					case 1:
						Bukkit.broadcastMessage("�7[�5Hero�fBattle�7] �6Das Spiel startet in einer Sekunde.");
						for(Player p : HeroBattle.getBattle().getPlayersIngame()){
							p.sendTitle("�5Hero �fGames", "�6startet in einer Sekunde");
						}
						break;
					case 0:
						if(HeroBattle.getBattle().getPlayersIngame().size() >= HeroBattle.getBattle().getMinPlayers()){
							for(Player p : HeroBattle.getBattle().getPlayersIngame()){
								p.setLevel(0);
								p.setExp(0);
								p.sendMessage("�7[�5Hero�fBattle�7] �6Du k�mpfst gegen " + "Test");
								p.sendTitle("�5Hero �fGames", "�6Viel Gl�ck!");
								}
							HeroBattle.getBattle().setGameState(GameState.INGAME);
						}
						else if(!repeat){
							Bukkit.broadcastMessage("�7[�5Hero�fBattle�7] �cEs sind nicht genug Spieler online, um das Spiel zu starten.");
							seconds = 60*5 + 1;
							repeat = true;
						}
						else{
							
							Bukkit.broadcastMessage("�7[�5Hero�fBattle�7] �cDa zuwenige Spieler online sind, wird der Server heruntergefahren.");
							Bukkit.getServer().shutdown();
						}
				}
				seconds--;
				}
		}, 0, 20);
	}
	
	@Override
	public void stop(){
		Bukkit.getScheduler().cancelTask(taskID);
	}

}
