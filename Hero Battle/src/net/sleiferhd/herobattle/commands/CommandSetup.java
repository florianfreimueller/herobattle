package net.sleiferhd.herobattle.commands;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import net.sleiferhd.herobattle.HeroBattle;
import net.sleiferhd.herobattle.map.Locations;

public class CommandSetup implements CommandExecutor{
	
	public boolean onCommand(CommandSender arg0, Command arg1, String arg2, String[] arg3) {
		if(arg0 instanceof Player){
			Player player = (Player)arg0;
			if(player.isOp()){
				if(arg1.getName().equalsIgnoreCase("setup")){
					if(arg3.length == 1){
						switch(arg3[0].toLowerCase()){
						case "arena":
							Locations.setArena(player.getLocation());
							HeroBattle.getBattle().getLogger().info("Arena was set");
							player.sendMessage("�7[�5Hero�fBattle�7] �aArena was set");
							return true;
						case "spawn":
							Locations.setSpawn(player.getLocation());
							HeroBattle.getBattle().getLogger().info("Spawn was set");
							player.sendMessage("�7[�5Hero�fBattle�7] �aSpawn was set");
							return true;
							default:
								player.sendMessage("�cSyntax: /setup <arena|spawn>");
								return false;
						}
					}
					player.sendMessage("�cSyntax: /setup <arena|spawn>");
					return false;
				}
			}
			player.sendMessage("�cYou don't have the permissions to execute this command!");
			return false;
		}
		arg0.sendMessage("�cYou need to be a player to execute this command!");
		return false;	
	}
	
}
