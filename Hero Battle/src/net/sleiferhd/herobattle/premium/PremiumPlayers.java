package net.sleiferhd.herobattle.premium;

import java.util.ArrayList;

import org.bukkit.entity.Player;

public class PremiumPlayers {
	private ArrayList<Player> premiumPlayerList;
	
	public PremiumPlayers(){
		init();
	}
	
	public void init(){
		premiumPlayerList = new ArrayList<>();
	}
	
	public boolean isPremium(Player p){
		for(Player player : premiumPlayerList)
			if(player.equals(p))
				return true;
		if(p.getPlayerListName().startsWith("�6")){
			premiumPlayerList.add(p);
			return true;
		}
		return false;
	}
	
	
}
