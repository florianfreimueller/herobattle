package net.sleiferhd.herobattle.configuration;

public class LobbyConfiguration extends Configuration{

	public LobbyConfiguration(String path) {
		super(path, null);
	}
	
	public void register(int min, int max){
		configuration.set("MinPlayers", min);
		configuration.set("MaxPlayers", max);
		saveFile();
	}
	
	public int getValue(String name){
		return configuration.getInt(name);
	}
	
}
