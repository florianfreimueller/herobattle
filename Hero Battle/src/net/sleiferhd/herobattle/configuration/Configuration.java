package net.sleiferhd.herobattle.configuration;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;

import lombok.Getter;

public abstract class Configuration {
	
	private File file;
	
	@Getter protected final FileConfiguration configuration;	
	
	protected Configuration(String path, String classpath) {
		file = new File("plugins/HeroBattleConfig", path);
		
		if (classpath != null) {
			if ( !exists() ) {
				try {
					loadConfiguration(classpath);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		}
		
		configuration = YamlConfiguration.loadConfiguration(file);
	}
	
	public Configuration(String path) {
		this(path, null);
	}
	
	public void saveFile() {
		try {
			configuration.save(file);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	protected void loadConfiguration(String classpath) throws Exception {
		InputStream stream = null;
		OutputStream resStreamOut = null;
		String jarFolder;
		
		try {
			stream = this.getClass().getClassLoader().getResourceAsStream(classpath);
			
			if (stream == null) return;
			
			int readBytes;
			byte[] buffer = new byte[4096];
			
			jarFolder = file.getPath().replace('\\', '/');
			
			resStreamOut = new FileOutputStream(jarFolder);
			
			while ((readBytes = stream.read(buffer)) > 0) {
				resStreamOut.write(buffer, 0, readBytes);
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			stream.close();
			resStreamOut.close();
		}
	}

	protected boolean exists() {
		return file.exists();
	}
	
}
