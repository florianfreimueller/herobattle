package net.sleiferhd.herobattle.configuration;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.World;

public class LocationConfiguration extends Configuration{

	public LocationConfiguration(String path) {
		super(path, null);
	}	
	
	public void setLocation(String name, Location loc){
		configuration.set(name + ".world", loc.getWorld().getName());
		configuration.set(name+".x", loc.getX());
		configuration.set(name+".y", loc.getY());
		configuration.set(name+".z", loc.getZ());
		configuration.set(name+".yaw", loc.getYaw());
		configuration.set(name+".pitch", loc.getPitch());
		saveFile();
	}
	
	public Location getLocation(String name){
		World w = Bukkit.getWorld(configuration.getString(name+".world"));
		double x = configuration.getDouble(name +".x");
		double y = configuration.getDouble(name +".y");
		double z = configuration.getDouble(name +".z");
		float yaw = (float) configuration.getDouble(name + ".yaw");
		float pitch = (float) configuration.getDouble(name + ".pitch");
		return new Location(w, x, y, z, yaw, pitch);
	}
}
