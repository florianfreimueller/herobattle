package net.sleiferhd.herobattle.gamestates;

public enum GameState {
	LOBBY, INGAME, ENDGAME
}
