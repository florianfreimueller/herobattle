package net.sleiferhd.herobattle.map;

import org.bukkit.Location;

import net.sleiferhd.herobattle.HeroBattle;

public class Locations {
	
	private static Location arena, spawn;
	
	public static void setArena(Location arg0) {
		HeroBattle.getBattle().getLocationConfig().setLocation("arena", arg0);
		arena = arg0;
	}
	
	public static void setSpawn(Location arg0) {
		HeroBattle.getBattle().getLocationConfig().setLocation("spawn", arg0);
		spawn = arg0;
	}
	
	public static Location getArena(){
		return arena;
	}
	
	public static Location getSpawn(){
		return spawn;
	}
}
