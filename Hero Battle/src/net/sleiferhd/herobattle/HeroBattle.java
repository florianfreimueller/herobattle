package net.sleiferhd.herobattle;

import java.util.ArrayList;
import java.util.logging.Logger;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;

import net.sleiferhd.herobattle.commands.CommandSetup;
import net.sleiferhd.herobattle.configuration.LobbyConfiguration;
import net.sleiferhd.herobattle.configuration.LocationConfiguration;
import net.sleiferhd.herobattle.countdown.LobbyCountdown;
import net.sleiferhd.herobattle.events.PlayerListUpdate;
import net.sleiferhd.herobattle.gamestates.GameState;
import net.sleiferhd.herobattle.map.Locations;
import net.sleiferhd.herobattle.premium.PremiumPlayers;

public class HeroBattle extends JavaPlugin {
	
	private static HeroBattle battle;
	private ArrayList<Player> playersIngame;
	private GameState currentGameState;
	private Logger logger;
	private int minPlayers, maxPlayers;
	private LobbyCountdown lobbycd;
	private PremiumPlayers premiums;
	private LocationConfiguration locConfig;
	private LobbyConfiguration lobbyConfig;
	
	@Override
	public void onEnable() {
		init();
		logger.info("[Hero Battle] The Hero Battle plugin was enabled.");
	}
	
	private void init(){
		battle = this;
		logger = Bukkit.getLogger();
		playersIngame = new ArrayList<>();
		currentGameState = GameState.LOBBY;
		premiums = new PremiumPlayers();
		lobbyConfig = new LobbyConfiguration("plugin/LobbyConfig.yml");
		
		minPlayers = lobbyConfig.getValue("MinPlayers");
		maxPlayers = lobbyConfig.getValue("MaxPlayers");
		if(minPlayers == 0){
			lobbyConfig.register(15, 80);
			minPlayers = lobbyConfig.getValue("MinPlayers");
			maxPlayers = lobbyConfig.getValue("MaxPlayers");
		}
		lobbycd = new LobbyCountdown();
		locConfig = new LocationConfiguration("plugin/Locations.yml");
		try{
			Locations.setSpawn(locConfig.getLocation("spawn"));
		}catch(Exception e){
			logger.info("[Hero Battle] You have to setup the spawn!");
		}
		try{
			Locations.setArena(locConfig.getLocation("arena"));
		}catch(Exception e){
			logger.info("[Hero Battle] You have to setup the arena!");
		}
		getCommand("setup").setExecutor(new CommandSetup());
		new PlayerListUpdate(battle);
	}
	
	public PremiumPlayers getPremiums(){
		return premiums;
	}
	@Override
	public void onDisable() {
		logger.info("[Hero Battle] The Hero Battle plugin was disabled.");
	}
	
	public ArrayList<Player> getPlayersIngame(){
		return playersIngame;
	}
	
	public static HeroBattle getBattle(){
		return battle;
	}
	
	public void setGameState(GameState state){
		currentGameState = state;
		gameStateUpdated(currentGameState);
	}
	
	public GameState getGameState(){
		return currentGameState;
	}
	
	private void gameStateUpdated(GameState gamestate){
		switch(gamestate){
		case LOBBY:
			try{
			for(Player p : playersIngame){
				p.teleport(Locations.getSpawn());
			}
			lobbycd.start();
			}catch(Exception e) {
				Bukkit.broadcastMessage("�7[�5Hero�fBattle�7] �cDie Lobby wurde nicht gesetzt!");
			}
			break;
		case INGAME:
			try{
			for(Player p : playersIngame){
				p.teleport(Locations.getArena());
			}
			lobbycd.stop();
			}catch(Exception e){
				Bukkit.broadcastMessage("�7[�5Hero�fBattle�7] �cDie Arena wurde nicht gesetzt!");			
			}
			break;
		case ENDGAME:
			for(Player p : playersIngame){
				p.teleport(Locations.getSpawn());
			}
			break;
		}
	}
	
	public void setMaxPlayers(int maxPlayers){
		this.maxPlayers = maxPlayers;
	}
	
	public void setMinPlayers(int minPlayers){
		this.minPlayers = minPlayers;
	}

	public int getMinPlayers() {
		return minPlayers;
	}

	public int getMaxPlayers() {
		return maxPlayers;
	}
	
	public LobbyCountdown getLobbyCountdown(){
		return lobbycd;
	}
	
	public LocationConfiguration getLocationConfig(){
		return locConfig;
	}
}
