package net.sleiferhd.herobattle.events;

import org.bukkit.event.Listener;
import org.bukkit.event.entity.PlayerDeathEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.GameMode;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;

import net.sleiferhd.herobattle.HeroBattle;
import net.sleiferhd.herobattle.gamestates.GameState;
import net.sleiferhd.herobattle.map.Locations;

public class PlayerListUpdate implements Listener{
	
	public PlayerListUpdate(HeroBattle battle){
		battle.getServer().getPluginManager().registerEvents(this, battle);
	}
	
	@EventHandler
	public void onJoin(PlayerJoinEvent ev){
		HeroBattle.getBattle().getPlayersIngame().add(ev.getPlayer());
		ev.setJoinMessage("�7[�a+�7] �a"+ev.getPlayer().getName());
		if(HeroBattle.getBattle().getGameState().equals(GameState.LOBBY))
			try{
				ev.getPlayer().teleport(Locations.getSpawn());
			}catch(Exception e){
				if(!ev.getPlayer().isOp())
				ev.getPlayer().kickPlayer("�cDer Spawn wurde noch nicht gesetzt!");
				else
					ev.getPlayer().sendMessage("�cDu musst den Spawn noch setzen!");
			}
		if(HeroBattle.getBattle().getPlayersIngame().size() == 1)
			HeroBattle.getBattle().getLobbyCountdown().start();
	}
	
	@EventHandler
	public void onLeave(PlayerQuitEvent ev){
		HeroBattle.getBattle().getPlayersIngame().remove(ev.getPlayer());
		ev.setQuitMessage("�7[�c-�7] �c"+ev.getPlayer().getName());
		if(HeroBattle.getBattle().getPlayersIngame().size() == 0)
			HeroBattle.getBattle().getLobbyCountdown().stop();
	}
	
	@EventHandler
	public void onPlayerDeath(PlayerDeathEvent ev){
		Player p = ev.getEntity();
		if(HeroBattle.getBattle().getPremiums().isPremium(p) && HeroBattle.getBattle().getGameState().equals(GameState.INGAME))
			p.setGameMode(GameMode.SPECTATOR);
		else if(HeroBattle.getBattle().getGameState().equals(GameState.INGAME))
			p.kickPlayer("�7[�5Hero�fBattle�7] �aDu bist gestorben, deshalb wurdest du in die Lobby gekickt.");
			
	}

}
